"let strict";

global.$ = require("jquery");
require("bootstrap");
const isElectron = require("is-electron")();
const settings = require("./settings.js");
global.accgen_settings = settings;
const dynamic = require("./dynamicloading.js");
const generation = require("./generation.js");
const gmail = require("./gmail.js");

global.extend = function (obj, src) {
    for (var key in src) {
        if (src.hasOwnProperty(key)) obj[key] = src[key];
    }
    return obj;
}

global.httpRequest = function (options, proxy, cookies, timeout) {
    return new Promise(async function (resolve, reject) {
        if (typeof document.proxiedHttpRequest == "undefined" || !proxy)
            $.ajax(extend({
                success: function (returnData) {
                    resolve(returnData);
                },
                error: function (xhr, status, error) {
                    console.error(xhr, error);
                    reject(xhr.responseJSON || xhr.responseText, error);
                }
            }, options));
        else {
            document.proxiedHttpRequest(options, proxy, cookies, resolve, reject, timeout);
        }
    });
}

// Code to communicate with the addon
var g_id = 0;
function messageAddon(data) {
    var id = g_id++;
    document.dispatchEvent(new CustomEvent("addon", { "detail": { id: id, data: data } }));
    return new Promise(function (resolve, reject) {
        document.addEventListener("addon_reply", function (event) {
            resolve(JSON.parse(event.detail).data);
        });
    });
}

var gen_status_text_priority = 0;

function change_gen_status_text(text, priority) {
    if (!priority)
        priority = 0;
    if (priority >= gen_status_text_priority) {
        if (text) {
            $("#generate_status").text(text);
            gen_status_text_priority = priority;
        } else
            gen_status_text_priority = 0;
    }
}

function displayerror(errortext) {
    if (errortext) {
        $("#generic_error").show("slow");
        $("#generic_error").text(errortext);
    } else
        $("#generic_error").hide("slow");
}

function parseSteamError(code, report, proxymgr) {
    switch (code) {
        case 13:
            return {
                error: 'The email chosen by our system was invalid. Please Try again.'
            };
        case 14:
            return {
                error: 'The account name our system chose was not available. Please Try again.'
            };
        case 84:
            if (proxymgr)
                proxymgr.proxy.ratelimit();
            return {
                error: 'Steam is limitting account creations from your IP or this email address (if using Gmail). Try again later.'
            };
        case 101:
            return {
                error: 'Captcha provider solved the captcha incorrectly!'
            };
        case 105:
            if (proxymgr && report)
                proxymgr.proxy.ban();
            return {
                error: proxymgr && !proxymgr.proxy.emulated ? "Proxy IP banned by steam. Removed from proxy list." : "Your IP is banned by steam. Try disabling your VPN."
            };
        case 17:
            if (report)
                report_email();
            if (!proxymgr)
                $("#generate_error_emailprovider").show();
            return {
                error: 'Steam has banned the domain. Please use Gmail or Custom domain'
            };
        case 1:
            return {};
        default:
            return {
                error: `Error while creating the Steam account! Steam error code ${code}!`
            };
    }
}

var proxylist = {
    proxylist: [],
    getProxy: function () {
        var proxies = this.proxylist;
        var time = Date.now();
        // Filter out proxies that are on timeout or banned
        proxies = proxies.filter(function (value) {
            return !value.banned && !value.errored && (!value.timeout || value.timeout < time);
        })
        // sort proxies, verified goes to the top
        proxies.sort(function (a, b) {
            if (a.verified == b.verified)
                return 0;
            if (a.verified)
                return -1;
            if (b.verified)
                return 1;
            return 0;
        })
        return {
            proxy: $.extend(proxies[0], {
                verify: function () {
                    this.verified = true;
                    this.errorcount = 0;
                    proxylist.dump();
                },
                ratelimit: function () {
                    this.timeout = Date.now() + 12 * 60 * 60 * 1000;
                    proxylist.dump();
                },
                ban: function () {
                    // proxy is banned
                    this.banned = true;
                    proxylist.dump();
                },
                error: function () {
                    if (!this.verified) {
                        this.errored = true;
                    } else {
                        if (this.errorcount >= 1)
                            this.errored = true;
                        else {
                            this.timeout = Date.now() + 12 * 60 * 60 * 1000;
                            this.errorcount = 1;
                        }
                    }
                    proxylist.dump();
                }
            })
        }
    },
    import: function (text) {
        var proxies = text.split("\n");
        for (var i in proxies) {
            var err = false;
            var proxy = proxies[i];
            try {
                new URL(proxy);
            } catch (error) {
                err = true;
            }
            if (!err) {
                if (this.proxylist.find(o => o.uri == proxy))
                    continue;
                this.proxylist.push({
                    uri: proxy
                })
            }
        }
        proxylist.dump();
    },
    dump: function () {
        localStorage.setItem("proxylist", JSON.stringify(this.proxylist))
    },
    load: function () {
        var data = localStorage.getItem("proxylist");
        if (proxylistLinter(data))
            this.proxylist = JSON.parse(data);
    }
}

global.edit_proxy_json = function () {
    if (proxylist.proxylist.length > 0)
        $("#proxy_json_textbox").val(JSON.stringify(proxylist.proxylist, null, 4));
    else
        $("#proxy_json_textbox").val('');
    $("#proxy_json").modal('show');
}

global.copyDetails = async function (id) {
    var data;
    data = $(`#${id}`).text();
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(data).select();
    document.execCommand("copy");
    $temp.remove();
    $(`#${id}`).text("Copied!");
    await sleep(1000);
    $(`#${id}`).text(data);
}

function proxylistLinter(list) {
    // Verify if the proxy list is valid
    var data;
    try {
        data = JSON.parse(list)
    } catch (e) {
        return false;
    }

    // Check if the json is correct
    if (Array.isArray(data)) {
        for (var i in data) {
            var entry = data[i];
            if (!entry.uri) {
                return false;
            }
            try {
                new URL(entry.uri);
            } catch (error) {
                return false;
            }
        }
    } else
        return false;
    return true;
}

global.save_proxy_json = function () {
    var data = $("#proxy_json_textbox").val();
    $("#proxy_json").modal('hide');
    if (data == "") {
        proxylist.proxylist = [];
        proxylist.dump();
        return;
    }

    if (!proxylistLinter(data)) {
        displayerror("Invalid format!");
        return;
    } else {
        proxylist.proxylist = JSON.parse(data);
        proxylist.dump();
        $("#proxy_json").modal('hide');
    }
}

global.proxy_list_save = function () {
    proxylist.import($("#proxy_list_input").val())
    $('#proxy_list_input').val('')
}

function parseErrors(data, report) {
    if (!data || (!data.success && !data.error.message && !data.error.steamerror)) {
        return "Unknown error!"
    }
    if (data.success == true)
        return;
    if (data.error && data.error.message)
        return data.error.message;
    if (data.error && data.error.steamerror)
        return parseSteamError(data.error.steamerror, report, data.proxymgr).error;
    return "Unknown error!"
}

function registerevents() {
    var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
    var eventer = window[eventMethod];
    var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

    eventer(messageEvent, async function (e) {
        if (e.origin != "https://store.steampowered.com")
            return
        if (e.data == "recaptcha-setup")
            return;
        if (typeof e.data !== 'string' || e.data.length < 200)
            return;
        // addon is out of date?
        if (e.data.split(";").length != 2) {
            alert("Invalid data received from addon");
            return;
        }

        change_visibility(true);
        var recap_token = e.data.split(";")[0];
        var account = (await generation.generateAccounts(1, null, recap_token, null, function statuscb(msg, id, ret) {
            change_gen_status_text(msg);
            if (ret)
                displayData(ret);
        }))[0];
        // Find errors and report banned domain if accgen email service in use
        var error = parseErrors(account, settings.get("email_provider") == "accgen");
        if (error) {
            displayData({
                parsederror: error,
                done: true
            });
            return;
        }
        displayData(account);
    }, false);
}

async function doRecapV3(action) {
    await dynamic.loadRecaptchaV3();
    return await grecaptcha.execute('6LfG55kUAAAAANVoyH7VqYns6j_ZpxB35phXF0bM', {
        action: action
    });
}

function report_email(email) {
    doRecapV3("vote_email").then(function (token) {
        $.ajax({
            url: '/userapi/recaptcha/bademail/' + token
        }).done(function (emailresp) {
            console.log("Log: email reported ban");
        })
    })
}

var electronStatusOnly;
function on_status_received(resp) {
    if (resp.electron) {
        if (resp.status)
            electronStatusOnly = true;
        else
            electronStatusOnly = false;
    } else {
        if (electronStatusOnly)
            return;
    }

    if (resp.status) {
        Promise.all([
            import(/* webpackChunkName: "status" */ "autolinker"),
            import(/* webpackChunkName: "status" */ "escape-html"),
        ]).then(([Autolinker, escape]) => {
            // Never trust anyone
            var out = escape.default(resp.status);
            out = Autolinker.default.link(out, { email: false, phone: false });
            document.getElementById("accgen_status_msg").innerHTML = out;
            $("#accgen_status").show("slow");
        });
    } else {
        $("#accgen_status").hide("slow");
    }
}

function perform_status_check() {
    $.ajax({
        url: "/api/v1/status"
    }).done(function (resp) {
        on_status_received(resp)
    })
}

global.sleep = function (ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function isMobile() {
    var check = false;
    (function (a) {
        if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true;
    })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
};

function isIOS() {
    return /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

}

function GetBrowser() {
    if (typeof InstallTrigger !== 'undefined')
        return "Firefox";
    if (!!window.chrome && (!!window.chrome.webstore || !!window.chrome.runtime))
        return "Chrome";
    var isIE = /*@cc_on!@*/ false || !!document.documentMode;
    if (isIE && !!window.StyleMedia)
        return "Edge";
    if ((!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0)
        return "Opera";
    return "Unsupported";
}

function AddonsNotSupported() {
    window.location = "legacy.html";
}

function changeText(isinstalled) {
    if (isinstalled) {
        $("#email_service_option_gmail > img")
            .css("opacity", "0.4")
            .css("filter", "alpha(opacity=40)");
        $("#email_service_option_gmail > figcaption > p").text("You need addon version 5.0 or above to enable gmail support.");
        return;
    }
    switch (GetBrowser()) {
        case "Firefox":
            if (isIOS())
                return AddonsNotSupported("Firefox IOS")
            document.getElementById("addon_download_text").textContent = "You don't have our Firefox addon yet!";
            document.getElementById('ffaddon').href = 'https://addons.mozilla.org/firefox/addon/sag/';
            document.getElementById('ffaddon').target = "_blank";
            document.getElementById('ffaddon').onclick = "";
            break;
        case "Chrome":
            if (isMobile())
                return AddonsNotSupported("Chrome Mobile");
            document.getElementById("addon_download_text").textContent = "You don't have our Chrome addon yet!";
            document.getElementById('ffaddon').href = 'https://chrome.google.com/webstore/detail/sag/piljlfgibadchadlhlcfoecfbpdeiemd';
            document.getElementById('ffaddon').target = "_blank";
            document.getElementById('ffaddon').onclick = "";
            break;
        case "Opera":
            if (isMobile())
                return AddonsNotSupported();
            document.getElementById("addon_download_text").textContent = "You don't have our Opera addon yet!";
            document.getElementById('ffaddon').href = 'https://addons.opera.com/en/extensions/details/sag/';
            document.getElementById('ffaddon').target = "_blank";
            document.getElementById('ffaddon').onclick = "";
            break;
        case "Yandex":
            if (isIOS())
                return AddonsNotSupported("Yandex IOS")
            document.getElementById("addon_download_text").textContent = "You don't have our Yandex addon yet!";
            document.getElementById('ffaddon').href = 'https://chrome.google.com/webstore/detail/sag/piljlfgibadchadlhlcfoecfbpdeiemd';
            document.getElementById('ffaddon').target = "_blank";
            document.getElementById('ffaddon').onclick = "";
            break;
        default:
            return AddonsNotSupported();
    }
}

/*Automatic generation*/

async function getRecaptchaSolution() {
    var captcha_key = settings.get("captcha_key");
    var captcha_host = settings.get("captcha_host")
    var res = await httpRequest({
        url: `${captcha_host}/in.php?key=${captcha_key}&method=userrecaptcha&googlekey=6LerFqAUAAAAABMeByEoQX9u10KRObjwHf66-eya&pageurl=https://store.steampowered.com/join/&header_acao=1&soft_id=2370&json=1`
    }).catch(function (err) {
        console.log(err);
        throw new Error("Failed to connect to Captcha provider");
    });
    try {
        if (typeof res == 'string')
            res = JSON.parse(res);
    } catch (err) {
        throw new Error("Captcha Service send invalid json!");
    }

    if (!res.request)
        throw new Error("Captcha Provider sent invalid json!");
    console.log("Captcha Service requestid: " + res.request);
    await sleep(10000);
    for (var i = 0; i < 30; i++) {
        await sleep(5000);
        var ans_res = await httpRequest({
            url: `${captcha_host}/res.php?key=${captcha_key}&action=get&id=${res.request}&json=1&header_acao=1`
        })
        try {
            if (typeof ans_res == 'string')
                ans_res = JSON.parse(ans_res);
        } catch (err) {
            throw new Error("Captcha Service send invalid json!");
        }
        console.log(ans_res)
        // Status could be error, 0 = not yet ready
        if (ans_res.status == 0)
            continue;
        res = ans_res;
        break;
    }
    if (res.status == 1)
        return res.request;
    else
        throw new Error("Captcha Service error!");
}

global.mass_generate_clicked = async function () {
    // Inline to limit scope
    function alter_table(id, data) {
        if (isNaN(id)) {
            console.error("Invalid ID");
            return;
        }

        if (data.username) $('#status_table tr').eq(id).find('td').eq(1).html(data.username);
        if (data.password) $('#status_table tr').eq(id).find('td').eq(2).html(data.password);
        if (data.email) $('#status_table tr').eq(id).find('td').eq(3).html(data.email);
        if (data.status) $('#status_table tr').eq(id).find('td').eq(4).html(data.status);
    }

    // Get accounts to generate (count)
    var max_count = $("#mass_gen_count").val();
    var concurrency = $("#mass_gen_concurrency").val();
    if (!max_count || isNaN(max_count) || max_count < 1) {
        displayerror("Count must be a non 0 and non negative number!");
        return false;
    }
    if (!concurrency || isNaN(concurrency) || concurrency < 1) {
        displayerror("Concurrency must be a non 0 and non negative number!");
        return false;
    }

    $("#status_table").empty();
    // Preallocate table
    for (var i = 0; i < max_count; i++)
        $("#status_table").append(
            `<tr>
            <td>${i}</td>
            <td></td>
            <td></td>
            <td></td>
            <td>Waiting...</td>
            </tr>`
        );

    // Show progress bar and hide other things
    change_visibility(true);
    $('#generate_stop').show();
    $("#generation_status").show("slow");

    function statuscb(msg, id) {
        alter_table(id, {
            status: msg
        });
    }

    var captcha = {
        getCaptchaSolution: async function (id) {
            for (var i = 0; i < 2; i++) {
                try {
                    statuscb("Getting captcha solution...", id);
                    return await getRecaptchaSolution();
                } catch (error) {
                    statuscb("Getting captcha solution failed!", id);
                    console.error(error);
                    if (i == 0)
                        await sleep(3000);
                }
            }
        }
    }

    function generationcallback(account, id) {
        var error = parseErrors(account, settings.get("email_provider") == "accgen");
        if (error) {
            alter_table(id, {
                status: error
            })
            return;
        }
        alter_table(id, {
            status: "Finished!",
            username: account.account.login,
            password: account.account.password,
            email: account.account.email
        });
        addToMassHistory(account.account);
    }

    var proxy = $("#proxy_check:checked").val() && typeof document.proxiedHttpRequest != "undefined" ? proxylist : undefined;
    if (!proxy) {
        // emulate a proxylist
        proxy = {
            localproxy: {
                proxy: {
                    emulated: true,
                    uri: "emulated",
                    verify: function () {
                        this.verified = true;
                        this.errorcount = 0;
                    },
                    ratelimit: function () {
                        this.uri = undefined;
                    },
                    ban: function () {
                        this.uri = undefined;
                    },
                    error: function () {
                        if (!this.errorcount)
                            this.errorcount = 1;
                        else
                            this.errorcount++;
                        if (this.errorcount >= 3)
                            this.uri = undefined;
                    }
                }
            },
            getProxy: function () {
                return this.localproxy
            }
        }
    }

    var valid_accounts = [];
    var accounts = await generation.generateAccounts(max_count, proxy, captcha, concurrency, statuscb, generationcallback, change_gen_status_text);
    for (var i = 0; i < max_count; i++) {
        var account = accounts[i];
        var error = parseErrors(account, false);
        if (error) {
            alter_table(i, {
                username: "",
                password: "",
                email: "",
                status: error
            })
            continue;
        }
        alter_table(i, {
            status: "Completed!",
            username: account.account.login,
            password: account.account.password,
            email: account.account.email
        })
        if (account.account)
            valid_accounts.push(account.account);
    }
    change_visibility(false);
    if ($("#down_check:checked").val() && valid_accounts.length >= 1)
        download_account_list(valid_accounts);
    return false;
}

/*Automatic generation end*/

global.commonGeneratePressed = async function () {
    if (settings.get("captcha_key")) //2captcha key is set
    {
        change_visibility(2);
        $("#mass_generator").modal('show');
        return;
    }
    if ($("#steam_iframe").is(":hidden")) {
        change_visibility(2);
        document.getElementById('steam_iframe_innerdiv').src = "https://store.steampowered.com/join/";
    }
    else
        document.getElementById('steam_iframe_innerdiv').src = "about:blank";
    $("#steam_iframe").toggle("slow")
}

global.selectEmailServicePressed = function () {
    dynamic.loadEmailServiceImages();
    $("#email_service_modal").modal('show');
}

global.commonChangeVisibility = function (pre_generate) {
    if (pre_generate) {
        $('#mx_error').hide("slow");
        $('#gmail_error').hide("slow");
        $('#saved_success').hide("slow");
        $('#twocap_error').hide("slow");
        $('#generate_error').hide("slow");
        $("#generate_error_emailprovider").hide("slow");
        $('#generated_data').hide("slow");
        $('#history_list').hide("slow");
        $("#generation_status").hide("slow");
        $('#generate_stop').hide("slow");
        displayerror(undefined);

        // Unload the steam recaptcha
        $('#steam_iframe').hide("slow");
        document.getElementById('steam_iframe_innerdiv').src = "about:blank";

        if (pre_generate == 1) {
            $('#control_buttons').hide();
            $('#generate_progress').show("slow");
        }
    } else {
        $('#control_buttons').show();
        $('#generate_progress').hide("slow");
    }
}

function addToHistory(acc_data) {
    if (localStorage.getItem("genned_account") == null) {
        localStorage.setItem("genned_account", JSON.stringify([]))
    }
    localStorage.setItem("genned_account", JSON.stringify(JSON.parse(localStorage.getItem("genned_account")).concat(acc_data)));
}

function addToMassHistory(acc_data) {
    if (localStorage.getItem("mass_genned_account") == null) {
        localStorage.setItem("mass_genned_account", JSON.stringify([]))
    }
    localStorage.setItem("mass_genned_account", JSON.stringify(JSON.parse(localStorage.getItem("mass_genned_account")).concat(acc_data)));
}

var lastacc;

function displayData(acc_data) {
    if (acc_data.done)
        change_visibility(false);

    if (acc_data.parsederror) {
        $("#generate_error").show("slow")
        $("#generate_error_text").html(acc_data.parsederror)
        return;
    }

    if (acc_data.done)
        addToHistory(acc_data.account);
    lastacc = acc_data;

    if (typeof document.startSteam != "undefined") {
        $("#electron_steam_signin").show();
    }

    if (acc_data.activation) {
        $("#acc_apps > span > strong").text(acc_data.activation.success.length + "/" + (acc_data.activation.success.length + acc_data.activation.fail.length));
        $("#acc_apps").show();
    } else {
        $("#acc_apps").hide();
    }

    $("#details_username").text(acc_data.account.login);
    $("#details_password").text(acc_data.account.password);

    $("#generated_data").show("slow");
}

global.electronSteamSignIn = function () {
    document.startSteam(lastacc);
    $("#electron_steam_signin").hide("slow");
}

async function isvalidmx(domain) {
    var patt = new RegExp("^([a-z0-9]+([\-a-z0-9]*[a-z0-9]+)?\.){0,}([a-z0-9]+([\-a-z0-9]*[a-z0-9]+)?){1,63}(\.[a-z0-9]{2,7})+$");
    if (!patt.test(domain))
        return false;
    var res = await new Promise(function (resolve, reject) {
        $.ajax({
            url: "/userapi/isvalidmx/" + domain,
            success: function (returnData) {
                resolve(returnData);
            },
            error: function () {
                reject();
            }
        });
    }).catch(function () {
        console.error('DNS lookup failed!');
    })
    if (!res || !res.valid)
        return false;
    return true;
}

function appSettingsInfo() {
    var rawApps = $("#settings_appids").val().match(/\d+(,$|)/g);
    if (!rawApps) {
        $("#acc_apps_setting > div > span").text("0");
        $("#acc_apps_setting > div").addClass("alert-warning");
        $("#acc_apps_setting > div").removeClass("alert-success");
        return;
    }

    $("#settings_appids").val(rawApps.join(","));

    var apps = $("#settings_appids").val().match(/\d+/g);

    $("#acc_apps_setting > div > span").text(apps.length);

    if (apps.length > 5) {
        $("#acc_apps_setting > div").addClass("alert-warning");
        $("#acc_apps_setting > div").removeClass("alert-success");
    } else {
        $("#acc_apps_setting > div").addClass("alert-success");
        $("#acc_apps_setting > div").removeClass("alert-warning");
    }
}

var lock_email_service_selection = false;

async function setProvider(provider) {
    settings.set("email_provider", provider);
    setTimeout(() => {
        $("#email_service_modal").modal('hide');
        $("#email_service_message").hide('slow');
        setTimeout(() => {
            lock_email_service_selection = false;
        }, 1000);
    }, 3000);
}

global.setUseAccgenMail = function () {
    if (lock_email_service_selection)
        return;
    lock_email_service_selection = true;
    $("#email_service_message > strong").text("Using accgen email service.");
    $("#email_service_message").show('slow');
    setProvider("accgen");
}

async function setupGmail() {
    if (lock_email_service_selection)
        return;
    lock_email_service_selection = true;
    console.log("Setup gmail clicked");
    // TODO: check for consent
    //show progress bar
    $("#email_service_progress").show('slow');
    $("#email_service_message > strong").text("Setting up gmail forwarding...");
    $("#email_service_message").show('slow');
    var result = await messageAddon({ task: "setupGmail" });
    console.log(result)
    if (!result.success) {
        $("#email_service_progress").hide('slow');
        $("#email_service_message > strong").text(`There was an issue setting up Gmail: ${result.reason || result.error}`)
        lock_email_service_selection = false;
        return;
    }
    var address = await gmail.getGmailAddress();
    if (!address) {
        $("#email_service_progress").hide('slow');
        $("#email_service_message > strong").text(`There was an issue setting up automated Gmail: Communication with gmail failed. Firefox users should disable "enhanced privacy protection" for this page.`);
        lock_email_service_selection = false;
        return;
    }
    settings.set("email_gmail", address);
    setProvider("gmailv2");
    $("#email_service_progress").hide('slow');
    $("#email_service_message > strong").text(`Automated gmail forwarding was set up for ${address}.`)
}

var changeurl_url = null;
global.changeurl = function (exit) {
    if (!exit)
        changeurl_url = window.event.target.href;
    else
        changeurl_url = "exit"
    if (generation.activegeneration) {
        if (generation.activegeneration > 1)
            $("#exit_page_modal_graceful").show();
        else
            $("#exit_page_modal_graceful").hide();
        if (exit) {
            if (!isElectron)
                $("#exit_page_modal_exit").hide();
            exit.preventDefault();
            exit.returnValue = '';
        }
        else
            $("#exit_page_modal_exit").show();
        $("#exit_page_modal").modal("show");
        return false;
    }
    return true;
}

global.common_init = function () {
    if (isElectron) {
        if (typeof document.ipc != "undefined") {
            document.ipc.on('alert-msg', (event, arg) => {
                on_status_received(arg);
            })
            document.ipc.send("ready");
            console.log("Ready sent!");
        }
        // https://github.com/sindresorhus/set-immediate-shim, setImmediate polyfill
        window.setImmediate = typeof setImmediate === 'function' ? setImmediate : (...args) => {
            args.splice(1, 0, 0);
            setTimeout(...args);
        };
        proxylist.load();
        $("#electron_ad").hide();
    }
    if (localStorage.getItem("genned_account") != null) {
        $('#history_button').show();
    }
    setInterval(perform_status_check, 10000);
    perform_status_check();
    registerevents();

    settings.convert();

    Promise.race([
        messageAddon({ task: "version" }),
        // Auto respond if it takes longer than 500 ms
        new Promise(function (resolve, reject) {
            setTimeout(resolve, 500);
        })
    ]).then(function (ret) {
        // Version older than 3.0, or not installed
        if (!ret) {
            // Check if addon installed using the old method, legacy
            $.ajax({
                url: "https://store.steampowered.com/join/"
            }).done(function () {
                console.log("Version older than 3.0 detected!");
                changeText(true);
                if (!settings.get("email_provider"))
                    selectEmailServicePressed();
            }).fail(function (resp) {
                changeText();
                $("#addon_dl").show();
                $("#accgen_ui").hide();
                $("#generate_button").hide();
                console.log("No addon installed!");
            });
        } else {
            console.log("Version 3.0 or above found!")
            if (!ret.apiversion || ret.apiversion < 2)
                changeText(true);
            else
                $("#email_service_option_gmail > img").click(setupGmail);
            if (!settings.get("email_provider"))
                selectEmailServicePressed();
        }
    })

    if (isElectron)
        $("#proxy-settings").show();

    // Add generator stop events and exit events
    $('#generate_stop > button').click(function () {
        $('#generate_stop').hide("slow");
        $(window).trigger("accgen.stopgeneration");
    });
    $("#exit_page_modal_graceful").click(function () {
        $(window).trigger("accgen.stopgeneration");
    });
    $("#exit_page_modal_exit").click(function () {
        generation.activegeneration = false;
        if (changeurl_url == "exit")
            window.close();
        else
            window.location = changeurl_url;
    });
    window.addEventListener('beforeunload', function (e) {
        global.changeurl(e);
    });


    appSettingsInfo();
    $("#settings_appids").on("input", appSettingsInfo);
}

function displayhistorylist(data, showdownloadhistory) {
    var shouldshow = data ? true : false;
    if (shouldshow) {
        change_visibility(2);
        $("#genned_accs").empty();
        $.each(data.reverse(), function (i, item) {
            $('<tr class="table-primary">').html(
                "<td>" + "<a href=\"https://steamcommunity.com/profiles/" + item.steamid + "\">" + item.login + "</a></td><td>" + item.password + "</td>").appendTo('#genned_accs');
        })
    }
    if (shouldshow) {
        $("#history_list").show('slow');
        if (showdownloadhistory)
            $("#history_download_button").show();
        else
            $("#history_download_button").hide();
    } else
        $("#history_list").hide('slow');
}

global.history_pressed = function () {
    if ($("#history_list").is(":hidden"))
        displayhistorylist(JSON.parse(localStorage.getItem("genned_account")), true);
    else
        displayhistorylist(undefined);
    return false;
}

//https://stackoverflow.com/a/45831280
function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();
    document.body.removeChild(element);
}

function download_account_list(accounts) {
    var s = "";
    for (var i = 0; i < accounts.length; i++) {
        s += (accounts[i].login + ":" + accounts[i].password) + "\r\n";
    }

    var date = new Date();
    download(`accounts–${date.getFullYear()}-${date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth()}-${date.getDate() < 10 ? "0" + date.getDate() : date.getDate()}.txt`, s);
}

global.history_download_pressed = function () {
    download_account_list(JSON.parse(localStorage.getItem("genned_account")));
    return false;
}

global.settings_help = function (page) {
    switch (page) {
        case "gmail":
            window.open("https://github.com/nullworks/accgen-web/wiki/Using-Your-Gmail-address-with-Steam-Account-Generator");
            break;
        case "mx":
            window.open("https://telegra.ph/file/5ceb2d563df573d3fa244.png");
            break;
        case "f2plist":
            window.open("https://steamdb.info/instantsearch/?refinementList%5Btags%5D%5B0%5D=Free%20to%20Play&refinementList%5BappType%5D%5B0%5D=Game");
            break;
        default:
            console.log("Invalid settings page");
            break;

    }
}

global.settings_pressed = function () {
    change_visibility(2);
    $("#settings_custom_domain").val(settings.get("email_domain"));
    $("#settings_twocap").val(settings.get("captcha_key"));
    $("#settings_caphost").val(settings.get("captcha_host"));
    $("#acc_steam_guard > input[type=\"checkbox\"]").prop("checked", settings.get("acc_steam_guard"));
    $("#acc_apps_setting > input[type=\"text\"]").val(settings.get("acc_apps_setting"));
    $("#settings_appids").trigger("input");
    return false;
}

global.custom_domain_clicked = async function () {
    if (lock_email_service_selection)
        return false;
    lock_email_service_selection = true;
    $('#custom_domain_service_modal').modal('show');
}

global.save_domain = async function () {
    $("#email_service_progress").show('slow');
    $("#email_service_message > strong").text("Setting up custom domain...");
    $("#email_service_message").show('slow');
    var custom_domain = $("#settings_custom_domain").val();
    if (custom_domain == "") {
        //settings.set("email_domain", null);
        lock_email_service_selection = false;
        $("#email_service_message").hide('slow');
        $("#email_service_progress").hide('slow');
        settings.set("email_domain", custom_domain);
    } else {
        if (!custom_domain.includes("@"))
            if (await isvalidmx(custom_domain)) {
            } else {
                $("#settings_custom_domain").val("");
                lock_email_service_selection = false;
                $("#email_service_message > strong").text("Your MX Settings are invalid. Please check Help for the correct settings. Note that DNS settings might take a few minutes to propagate.");
                $("#email_service_progress").hide('slow');
                return false;
            }
        $("#email_service_progress").hide('slow');
        settings.set("email_domain", custom_domain);
        $("#email_service_message > strong").text("Custom domain set up.");
        setProvider('custom_domain');
    }
}

global.save_clicked = async function () {
    settings.set("acc_steam_guard", $("#acc_steam_guard > input[type=\"checkbox\"]").prop("checked"));
    settings.set("acc_apps_setting", $("#acc_apps_setting > input[type=\"text\"]").val());

    var captcha_key = $("#settings_twocap").val();
    var captcha_host = ($("#settings_caphost").val() != '') ? $("#settings_caphost").val() : "https://2captcha.com";
    if (captcha_key != "") {
        var res = await httpRequest({
            url: `${captcha_host}/res.php?key=${captcha_key}&action=getbalance&header_acao=1`
        }).catch(function (err_response, error) {
            $("#twocap_error").show("slow");
            $("#settings_twocap").val("");
        });
        if (!res)
            return false;
        if (res == "ERROR_KEY_DOES_NOT_EXIST") {
            $("#twocap_error").show("slow");
            $("#settings_twocap").val("");
            return false;
        }
        $("#twocap_error").hide("slow");
        settings.set("captcha_key", captcha_key);
        settings.set("captcha_host", captcha_host);
    } else {
        $("#twocap_error").hide("slow");
        settings.set("captcha_key", null);
    }
    $("#saved_success").show("slow");
    return false;
}